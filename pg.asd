;;; -*- Mode: lisp -*-
;;

(defpackage #:pg-system (:use #:asdf #:cl))
(in-package #:pg-system)


(defclass pg-component (cl-source-file)
  ())

;; For CMUCL, ensure that the crypt library is loaded before
;; attempting to load the code. 
#+cmu
(defmethod perform :before ((o load-op) (c pg-component))
  (ext:load-foreign "/usr/lib/libcrypt.so"))

(defsystem :pg
    :name "Socket-level PostgreSQL interface"
    :author "Eric Marsden"
    :version "0.24"
    :depends-on (
              #+lispworks "comm"
              #+cormanlisp :sockets
              #+sbcl :sb-bsd-sockets
	      #+sbcl :sb-rotate-byte
              #+(and mcl (not openmcl)) "OPENTRANSPORT")
    :components ((:file "md5")
                 (:file "defpackage" :depends-on ("md5"))
                 (:pg-component "sysdep" :depends-on ("defpackage" "md5"))
                 (:file "meta-queries" :depends-on ("defpackage"))
                 (:file "parsers" :depends-on ("defpackage"))
                 (:file "utility" :depends-on ("defpackage"))
                 (:file "lowlevel" :depends-on ("defpackage"))
                 (:file "pg" :depends-on ("sysdep" "parsers"))
                 (:file "large-object" :depends-on ("pg"))
                 (:file "v2-protocol" :depends-on ("pg" "large-object" "utility"))
                 (:file "v3-protocol" :depends-on ("pg" "large-object" "utility"))))


