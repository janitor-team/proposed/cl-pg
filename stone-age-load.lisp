;;; stone-age-load.lisp
;;;
;;; Author: Eric Marsden <emarsden>
;;; Time-stamp: <2005-07-17 emarsden>

(load (compile-file "md5.lisp"))
(load "defpackage.lisp")
(load (compile-file "meta-queries.lisp"))
(load (compile-file "sysdep.lisp"))
(load (compile-file "parsers.lisp"))
(load (compile-file "utility.lisp"))
(load (compile-file "lowlevel.lisp"))
(load (compile-file "pg.lisp"))
(load (compile-file "large-object.lisp"))
(load (compile-file "v2-protocol.lisp"))
(load (compile-file "v3-protocol.lisp"))


;; EOF
