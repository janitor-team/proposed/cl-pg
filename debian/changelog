cl-pg (1:20061216-6) unstable; urgency=medium

  * Team upload.
  * Update Vcs-* fields for move to salsa.
  * Set Maintainer to debian-common-lisp@l.d.o.
  * Update d/watch using uscan git HEAD mode.
  * Use secure URL for Homepage.
  * Move to 3.0 (quilt) source format.
  * Remove Build-Depends on dh-lisp.
  * Remove obsolete README.building.

 -- Sébastien Villemot <sebastien@debian.org>  Sat, 07 Apr 2018 18:25:47 +0200

cl-pg (1:20061216-5) unstable; urgency=low

  * Added debian/README.building file 
  * Changed to the lisp Section
  * Added dummy watch file
  * Added ${misc:Depends} to Depends
  * Now use debhelper v7
  * Updated Standards-Version no real changes

 -- Peter Van Eynde <pvaneynd@debian.org>  Wed, 02 Sep 2009 13:49:38 +0100

cl-pg (1:20061216-4) unstable; urgency=low

  * Added Build-Depends-Indep for dh-lisp (Closes: #470320)

 -- Peter Van Eynde <pvaneynd@debian.org>  Fri, 14 Mar 2008 23:16:54 +0100

cl-pg (1:20061216-3) unstable; urgency=low

  * Changed to group maintanance
  * Added Vcs-Git control field
  * debhelper is Build-Depends
  * Updated standard version without real changes
  * documented how to update

 -- Peter Van Eynde <pvaneynd@debian.org>  Sun, 24 Feb 2008 15:53:49 +0100

cl-pg (1:20061216-2) unstable; urgency=low

  * Upload to unstable. 

 -- Peter Van Eynde <pvaneynd@debian.org>  Mon, 09 Apr 2007 01:04:52 +0200

cl-pg (1:20061216-1) experimental; urgency=low

  * New upstream. Major changes:

     + Don't flush the network stream in SEND-PACKET, because it slows down
     API calls that have multiple SEND-PACKET calls in them. From
     attila.lendvai@gmail.com.

     + Fix numeric parser for negative numbers. From
     levente.meszaros@gmail.com

     + Make sure we consume the ReadyForQuery packet that is generated when
     closing a prepared statement or portal, or the packet can be
     misinterpreted by a later query, leading to data loss. Fix from Robert J.
     Macomber.

     + Add an ABORT keyword argument to PG-DISCONNECT (from Robert J.
     Macomber <pgsql@rojoma.com>), as per CL:CLOSE.

      "I've run into a problem with pg-disconnect if something abnormal
      happens to the database connection -- if the database goes away for a
      restart while pg has a connection open, for example.  When this
      happens, pg-disconnect fails, and the socket file descriptor is left
      open (presumably for a finalizer to clean up), also raising a new
      error from the unwind-protect in with-pg-connection.  To guard against
      the possibility, I've added an :abort parameter to pg-disconnect, like
      cl:close has, and made with-pg-connection call it with :abort t if the
      body exits abnormally, in the same way that with-open-file operates.
      When :abort is true, the modified pg-disconnect closes the database
      connection ungracefully, including making the close call abort
      (otherwise, sbcl at keast tries to flush the stream, raising another
      error if the database isn't there anymore)."

     + Allow encoding used for socket communication with the backend to be
     specified as a keyword argument to PG-CONNECT, for cases where rebinding
     *PG-CLIENT-ENCODING* is inconvenient.

     Add a simple test for encoding support.

     (From Attila Lendvai <attila.lendvai@gmail.com>)
  * upload to experimental during the freeze

 -- Peter Van Eynde <pvaneynd@debian.org>  Tue,  6 Feb 2007 10:32:28 +0100

cl-pg (1:20061022-1) unstable; urgency=low

  * Added XS-X-Vcs-Darcs header
  * modified S-X-Vcs-Darcs to XS-Vcs-Darcs field
  * New upstream with many bugfixes 

 -- Peter Van Eynde <pvaneynd@debian.org>  Tue, 24 Oct 2006 15:03:38 +0200

cl-pg (1:20060919-1) unstable; urgency=low

  * New upstream version with several important bugfixes
  * Updated standards version without real changes. 

 -- Peter Van Eynde <pvaneynd@debian.org>  Wed, 20 Sep 2006 20:29:49 +0200

cl-pg (1:20060207-2) unstable; urgency=low

  [ René van Bevern ]
  * debian/control: get dependencies right: depend on ${misc:Depends}
    instead of common-lisp-controller explicitly and put debhelper to
    Build-Depends

 -- Peter Van Eynde <pvaneynd@debian.org>  Mon, 19 Jun 2006 07:58:55 +0200

cl-pg (1:20060207-1) unstable; urgency=low

  * Added missing Build-Depends on 'dh-lisp' (Closes: #351770)
  * New upstream
  * Added epoch to handle version stupidity 

 -- Peter Van Eynde <pvaneynd@debian.org>  Tue,  7 Feb 2006 16:15:30 +0100

cl-pg (20061225-1) unstable; urgency=low

  * New upstream
  * Better sbcl unicode support
  * Improved asdf compatibility
  * Documentation for pg-close-portal fixed in upstream 
    (Closes: #349778)

 -- Peter Van Eynde <pvaneynd@debian.org>  Wed, 25 Jan 2006 20:32:49 +0100

cl-pg (20050717-1) unstable; urgency=low

  * token new darcs version
  * unicode fix
  * New upstream.
  * Now no longer a native package to ease NMU and ubuntu related forks.

 -- Peter Van Eynde <pvaneynd@debian.org>  Mon,  1 Aug 2005 11:12:33 +0200

cl-pg (20040920) unstable; urgency=low

  * New upstream release:
    Add support for the SQL NUMERIC type, thanks to Risto Sakari Laakso.
  * Fixed Extended queries with never versions of postgreSQL

 -- Peter Van Eynde <pvaneynd@debian.org>  Mon, 20 Sep 2004 00:13:39 +0200

cl-pg (20040810) unstable; urgency=low

  * Now a debian-native package.
  * Support for the v3 protocol
  * Support for parse/bind/execute parts of that protocol
  * Support for COPY IN/OUT modes
  * New implementation fixes many bugs, among then: Closes: #244816 

 -- Peter Van Eynde <pvaneynd@debian.org>  Tue, 10 Aug 2004 13:08:31 +0200

cl-pg (0.19-1) unstable; urgency=low

  * New upstream.
  * Upstream has fixed write-sequence bug. Closes: #214963 

 -- Peter Van Eynde <pvaneynd@debian.org>  Wed, 22 Oct 2003 11:46:59 +0200

cl-pg (0.18-3) unstable; urgency=low

  * Now also supports sbcl. 

 -- Peter Van Eynde <pvaneynd@debian.org>  Fri, 18 Jul 2003 09:45:55 +0200

cl-pg (0.18-2) unstable; urgency=low

  * Changed section from libs to devel 

 -- Peter Van Eynde <pvaneynd@debian.org>  Tue,  3 Jun 2003 15:12:49 +0200

cl-pg (0.18-1) unstable; urgency=low

  *  New upstream release.

 -- Peter Van Eynde <pvaneynd@debian.org>  Fri, 23 May 2003 10:01:42 +0200

cl-pg (0.16-1) unstable; urgency=low

  * Changed architecture to all 
  * Initial Release. Closes: #170774
  * Actually based on unreleased 0.16 with a fix for a few cmucl bugs 

 -- Peter Van Eynde <pvaneynd@debian.org>  Tue,  3 Dec 2002 10:02:44 +0100

